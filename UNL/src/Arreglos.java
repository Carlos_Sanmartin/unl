
import java.io.InputStream;
import java.util.Scanner;

public class MenuSort 
{    
    private static Scanner sc;
    private static InputStream in = System.in;
    
    private int op;
    private boolean flag = true;
    
    private static final String BLUE = "\u001B[34m";
    private static final String BLACK = "\u001B[30m";
    private int lim;
    private int[][] vector;
    private int[] vectorSort;
    
    public MenuSort()
    {
        sc = new Scanner(in, "ISO-8859-1");
        op = 0;
    }
    
    public static  void viewVector(int[] v)
    {
        for (int w : v) 
        {
            System.out.print("["+BLUE+w+BLACK+"] ");
        }
        System.out.println("");
    }
    
    public int getIntScannerIn()
    {
        while(true)
        {
            try 
            {
                op = sc.nextInt();
                break;
            } catch (Exception e) 
            {
                System.out.println("## DATO ERRONEO ###");
            }
        }
        
        return op;
    }
      
    public void setVector()
    {
        vector = new int[][]{{7,3,5},{7,3,5},{7,3,5},{7,3,5}};
//        {7,6,4,9,5,3,2,9,8,5}};
//        System.out.print("Limite > : ");
//        lim = getIntScannerIn();
//        vector = new int[lim];

//        for (int i = 0; i < vector.length; i++) 
//        {
//            System.out.print("["+i+"] > ");
//            vector[i] = getIntScannerIn();
//        }        
    }
    
    public void sortVectorLow()
    {
        int buffer;
        vectorSort = vector;
            for (int i = 0; i < vectorSort.length-1; i++) 
                for (int j = 0; j < vectorSort.length-1; j++) 
                    if(vectorSort[j] > vectorSort[j+1])
                    {
                        buffer = vectorSort[j+1];
                        vectorSort[j+1] = vectorSort[j];
                        vectorSort[j] = buffer;
                    }
            viewVector(vectorSort);
    }
    
    public void sortVectorHigh()
    {
        int buffer;
        vectorSort = vector;
            for (int i = 0; i < vectorSort.length-1; i++) 
                for (int j = 0; j < vectorSort.length-1; j++) 
                    if(vectorSort[j] < vectorSort[j+1])
                    {
                        buffer = vectorSort[j+1];
                        vectorSort[j+1] = vectorSort[j];
                        vectorSort[j] = buffer;                        
                    }       
            viewVector(vectorSort);
    }
    
    public void init()
    {
        while (flag) 
        {            
            System.out.println("Ingresa una de las siguientes opciones");
            System.out.println("1. Ingresar Vector");
            System.out.println("2. Ordenar de Mayor a Menor");
            System.out.println("3. Ordenar de Menor a Mayor");
            System.out.println("3. Ver vector Original");
            System.out.println("5. Salir");
            
            System.out.print("Opción: ");
            
            getIntScannerIn();
            
            switch(op)
            {
                case 1:
                        setVector();
                    break;
                case 2:
                        sortVectorHigh();
                    break;
                case 3:
                        sortVectorLow();
                    break;
                case 4:
                        viewVector(vector);
                    break;
                case 5:
                        flag = false;
                    break;
               default:
                        System.out.println("### La opción No existe ###");
                    break;
            }
        }
    }
    
    public static void main(String[] args)
    {
        MenuSort menu = new MenuSort();
        menu.init();
    } 
}