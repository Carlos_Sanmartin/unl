/*
 * Copyright (C) 2016 2do A Programacion
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/**
 *
 * @author 2do A Programacion
 */

public class NewClass 
{    
    public static void main(String[] args)
    {                
    	int[] arreglo = new int[7];

    	arreglo[0] = 9;
    	arreglo[1] = 7;
    	arreglo[2] = 5;
    	arreglo[3] = 8;
    	arreglo[4] = 4;
    	arreglo[5] = 2;
    	arreglo[6] = 1;

    	int m;
    	int n;
    	int o;

        for (int i = 0; i < arreglo.length; i++) 
        {
            for (int j = 0; j < arreglo.length-1; j++) 
            {
                if ( arreglo[j] > arreglo[j+1])
                {
                    m = arreglo[j];
                    n = arreglo[j+1];

                    arreglo[j] = n;
                    arreglo[j+1] = m;
                }                    
            }
        }
        
        for (int i : arreglo) 
        {
            System.out.print(i+" - ");
        }
    }
}