/*
 * Copyright (C) 2016 2do A Programacion
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package Sort;

/**
 *
 * @author 2do A Programacion
 */
public abstract class SortDecorator implements Sort
{
    private static final String BLUE = "\u001B[34m";
    private static final String BLACK = "\u001B[30m";
    
    int[] vector, vectorSort;
    
    public SortDecorator(int[] vector)
    {
        setVector(vector);
    }
    
    public void setVector(int[] vecto)
    {
        this.vector = vector;
        vectorSort = vector;
    }
    
    public void viewVectorOriginal()
    {
        for (int i : vector)
            System.out.print(BLUE+"["+BLACK+i+BLUE+"] "+BLACK);
        System.out.println();
    }
    
    public void viewVectorSort()
    {
        for (int i : vector)
            System.out.print(BLUE+"["+BLACK+i+BLUE+"] "+BLACK);
        System.out.println();
    }
}
